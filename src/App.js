import React, { useState, useEffect }from 'react';
import Loginview from './components/Loginview';
import Customerview from './components/Customerview';
import Carview from './components/Carview';
import Addcarview from './components/Addcarview';
import Editcarview from './components/Editcarview';

function App() {
  const [loggedIn, setLoggedIn] = useState();
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const [carToEdit, setCarToEdit] = useState();
  let headers = new Headers();
  headers.set('Authorization','Basic ' + btoa(username + ':' + password));
  const [cars, setCars] = useState([]);
  const [customers, setCustomers] = useState([]);

  useEffect(() => {
    if (loggedIn) {
      const getCars = async () => {
        const carsFromServer = await fetchCars()
        setCars(carsFromServer);
      }
      getCars();
      const getCustomers = async () => {
        const customersFromServer = await fetchCustomers()
        setCustomers(customersFromServer);
      }
      getCustomers();
    }
  }, [loggedIn])

  const fetchCars = async () => {
    headers.append('Content-Type', 'text/json');
    headers.set('Authorization','Basic ' + btoa('user:password'));
    const res = await fetch('http://localhost:8081/api/v1/cars', {
      method:'GET',
      headers:headers
    })
    const data = await res.json();
    return data;
  }

  const fetchCustomers = async () => {
    headers.append('Content-Type', 'text/json');
    headers.set('Authorization','Basic ' + btoa(username + ':' + password));
    const res = await fetch('http://localhost:8081/api/v1/customers', {
      method:'GET',
      headers:headers
    })
    const data = await res.json();
    return data;
  }

  const logIn = (username, password) => {
    setUsername(username);
    setPassword(password);
    if (username === "admin" && password === "admin") {
      setLoggedIn(true);
    } else {
      alert("Fel användarnamn eller lösenord");
    }
  }
  const logOut = () => {
    setLoggedIn(false);
  }

  const saveCar = async (brand, size, price) => {
    let car = {
      brand: brand,
      size: size,
      price: price
    }
    headers.append('Content-Type', 'application/json');
    const res = await fetch('http://localhost:8081/api/v1/addcar', {
      method:'POST',
      headers:headers,
      body: JSON.stringify(car)
    })
    const data = await res.json()
    if (res.status === 200) {
      alert("Bil skapad");
    }
  }

  const saveEditedCar = async (car) => {
    headers.append('Content-Type', 'application/json');
    const res = await fetch('http://localhost:8081/api/v1/updatecar', {
      method:'PUT',
      headers:headers,
      body: JSON.stringify(car)
    })
    const data = await res.json()
    if (res.status === 200) {
      alert("Bil uppdaterad");
    }
  }

  const editCar = (car) => {
    setCarToEdit(car);
  }

  const deleteCar = async (carID) => {
    headers.append('Content-Type', 'application/json');
    const res = await fetch(`http://localhost:8081/api/v1/deletecar/${carID}`, {
      method:'DELETE',
      headers:headers
    })
    const data = await res.json()
    if (res.status === 200) {
      alert("Bil raderad");
    }
  }

  return (
    <div className="container">
      {loggedIn === false || loggedIn === undefined ?
      <Loginview logIn={logIn} username={username} password={password} /> :
      <>
        <div className="links">
            <ul>
                <div className="logout-button">
                <button type="button" className="standard-button" onClick={logOut}>Logga ut</button>
            </div>
            </ul>
        </div>
      <Customerview customers={customers}/> <br/>
      <Carview cars={cars} editCar={editCar} deleteCar={deleteCar}/><br/>
      <Addcarview saveCar={saveCar}/><br/>
      <Editcarview carToEdit={carToEdit} saveEditedCar={saveEditedCar}/> </> }
    </div>
  );
}

export default App;
