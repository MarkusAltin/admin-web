import React from 'react'

export default function Customerview({customers}) {
    return (
        <form>
        <div className="customers">
        <h4 id="columns">Kunder</h4>
        <h4 id="columns">ID&emsp;Namn&emsp;&emsp;Adress</h4>
            {customers.map((customer) => (
                <div key={customer.id} className="textfields">
                    <h4 id="table" >{customer.id}&emsp;{customer.name}&emsp;{customer.adress}</h4>
                </div>
            ))}
        </div>
        </form>
    )
}
