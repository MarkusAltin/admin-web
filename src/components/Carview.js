import React from 'react'

export default function Carview({cars, deleteCar, editCar}) {
    
    return (
        <form>
        <div className="cars">
            <h4 id="columns">Bilar</h4>
        <h4 id="columns">ID&ensp;Märke&ensp;Storlek&ensp;Pris</h4>
            {cars.map((car) => (
                <div key={car.id} className="textfields">
                    <h4 id="table" >{car.id}&emsp;{car.brand}&emsp;{car.size}&emsp;{car.price}</h4>
                    <div className="button">
                        <button type="button" className="submit" onClick={() => editCar(car)}>Redigera</button>
                        <button type="button" className="negative" onClick={() => deleteCar(car.id)}>Radera</button>
                    </div>
                </div>
            ))}
        </div>
        </form>
    )
}
