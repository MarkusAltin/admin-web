import React from 'react'

export default function Editcarview({carToEdit, saveEditedCar}) {
    let brand;
    let size;
    let price;


    function handleBrand (event) {
        brand = event.target.value;
        carToEdit.brand = brand;
    }
    const handleSize = event => {
        size = event.target.value;
        carToEdit.size = size;
    }
    const handlePrice = event => {
        price = event.target.value;
        carToEdit.price = price;
    }

    return (
        <form>
           {carToEdit === undefined ? '' :
        <div className="form-div">
           <div className="textfields">
                <label id="orderid">Car ID: {carToEdit.id}</label><br/><br/>
                <input type="text" placeholder={carToEdit.brand} onChange={handleBrand}/><br/><br/>
                <input type="text" placeholder={carToEdit.size} onChange={handleSize}/><br/><br/>
                <input type="text" placeholder={carToEdit.price} onChange={handlePrice}/>
                <br/><br/>
            </div><br/>
        <div className="button">
            <button type="submit" className="submit" onClick={() => saveEditedCar(carToEdit)}>Spara</button>
        </div>
       </div> }
    </form>
    )
}
