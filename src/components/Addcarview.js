import React from 'react'

export default function Addcarview({saveCar}) {
    let brand;
    let size;
    let price;

    function handleBrand (event) {
        brand = event.target.value;
    }
    const handleSize = event => {
        size = event.target.value;
    }
    const handlePrice = event => {
        price = event.target.value;
    }

    return (
        <form>
        <div className="form-div">
            <h4 id="columns">Lägg till bil</h4>
           <div className="textfields">
           <input type="text" placeholder="Märke" onChange={handleBrand}/><br/><br/>
           <input type="text" placeholder="Storlek" onChange={handleSize}/><br/><br/>
           <input type="text" placeholder="Pris" onChange={handlePrice}/>
            </div><br/>
        <div className="button">
            <button type="submit" className="submit" onClick={() => saveCar(brand, size, price)}>Lägg till</button>
        </div>
       </div> 
    </form>
    )
}

