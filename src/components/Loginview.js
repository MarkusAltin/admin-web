import React from 'react'

export default function Loginview({logIn, username, password}) {

    function handleUsername (event) {
        username = event.target.value;
    }
    const handlePassword = event => {
        password = event.target.value;
    }

    return (
        <form>
        <div className="form-div">
           <div className="textfields">
               <input type="text" placeholder="Användarnamn" onChange={handleUsername}/><br />
               <input type="password" placeholder="Lösenord" onChange={handlePassword}/><br />
           </div>
       <div className="button">
           <button type="submit" className="submit" onClick= {() => logIn(username, password) }>Logga in</button>
       </div>
       </div>
   </form>
    )
}
